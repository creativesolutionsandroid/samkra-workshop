package com.cs.samkraworkshop.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.samkraworkshop.Constants;
import com.cs.samkraworkshop.R;
import com.cs.samkraworkshop.Utils.TouchImageView;
import com.jsibbold.zoomage.ZoomageView;

import java.util.ArrayList;

/**
 * Created by BRAHMAM on 21-11-2015.
 */
public class BigImagesAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<String> images;
    Context context;


    public BigImagesAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.images = images;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1.0f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_big_image, container, false);

        ZoomageView store_image = (ZoomageView) itemView.findViewById(R.id.image);
        Glide.with(context).load(Constants.USER_IMAGES +images.get(position)).into(store_image);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}