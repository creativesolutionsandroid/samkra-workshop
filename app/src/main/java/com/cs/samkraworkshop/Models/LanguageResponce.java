package com.cs.samkraworkshop.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class LanguageResponce {


    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String messageAr) {
        MessageAr = messageAr;
    }

    public static class Data {
        @Expose
        @SerializedName("Language")
        private String Language;
        @Expose
        @SerializedName("UserId")
        private int UserId;
    }
}
